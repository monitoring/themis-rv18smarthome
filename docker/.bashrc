# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '

export THBIN=/home/user/third
export LTLMON=${THBIN}/bin
export LTL2MON_HOME=${THBIN}/ltl3tools-0.0.7
export SPOT=${THBIN}/spot
export RTLCONV=${THBIN}/lamaconv

export LD_LIBRARY_PATH=$SPOT/lib:$LD_LIBRARY_PATH
export CPATH=$SPOT/include:$CPATH
export PATH=$SPOT/bin:${RTLCONV}:$LTLMON:$PATH
