---
title: Plots
---

This folder contains the necessary scripts needed to generate the ADL schedule and plots.

## Dependencies
> **If you are running inside the docker, the dependencies are already installed**.

We use [`R`](https://www.r-project.org/) to compile our data with the following library dependencies:

* ggplot2
* reshape2
* dplyr
* xtable  

You can run the following to install dependencies if you have R already:
```
cd ../../docker
sudo Rscript install.r
```

For metrics, the database format used is Sqlite3, this will require that the tool `sqlite3` be invoked to extract the information as csv from the database.

## Note on Generated Files for Docker Users

The generated files (PDFs) from the scripts will be stored in the local folder. It is possible to open them from outside docker in the same directory.

## Plotting the ADL Schedule

The [adl.r](adl.r) script plots an ADL schedule from a given trace.
As such, the plot requires that [trace.log](../trace.log) be present.
This file is generated whenever the case study is run (to run the case study [go here](..)).


If you choose to simply plot a schedule without running any replay, [a trace](../trace.zip) is provided with you but is compressed.
To unpack the trace simply execute the following:

```
cd ..
unzip trace.zip
cd plot
```

A [trace.log](../trace.log) file will be generated.


To generate the ADL schedule from a given trace execute:

```
make adl
```

This generates a large PDF file for the trace that shows ADL schedule.

## Plotting the Metrics

The [perf.r](perf.r) script plots the performance graphs based on the recorded data of the case study.
This will plot the performance of targeted scenarios using the database [orange.db](../orange.db) in the [case study folder](..).

> This requires a sample size of 5 for each scenario (simply modify the sample size in [perf.r](perf.r) if you want to use different values).

To generate the plots execute:

```
make perf
```

## Cleaning up

Use `make clean` to remove all generated files.
