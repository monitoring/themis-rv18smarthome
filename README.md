---
title: Artifact Repostiory - Bringing Runtime Verification Home - Hierarchical Monitoring of Smart Homes
---

## Downloading the artifact

To download the artifact simply clone the repository.

```
git clone https://gitlab.inria.fr/monitoring/themis-rv18smarthome.git
```

## Reading the README.md Files ##
All README.md files are pre-rendered as HTML for convenience.
In each directory (including this one), the `index.html` file corresponds to a rendered `README.md`.


## Exploring the Artifacts


### Using the Docker Container
An environment for exploring the artifacts is provided as a docker container with all needed dependencies.
We provide the Dockerfile to build the image.
For more instructions see the [docker](docker/) folder.


### Setup without Docker
If you wish to run the tool without docker.

* Download and extract all third-party dependencies in [third.tgz](docker/third.tgz).
* Configure the environment variables (add all exports from [.bashrc](docker/.bashrc) to your `.bashrc` or startup shell).
* See the dependencies section in [case](case/).


## Exploring the Case Study
The main case study is found under [case](case), check the sub-folder `README.md` files for instructions on exploring and reproducing the experiments.


We bring your attention to the following:

* [case](case): contains all files relevant to the case study including software and data.
* [case/plot](case/plot): the necessary plotting scripts for recreating all graphs in the paper.
* [case/trace.zip](case/trace.zip) the trace we used to verify the precision and recall.
* [case/orange.db](case/orange.db) the statistics from trace replay to capture communication and computation metrics.
* [case/specs](case/specs) the specifications used throughout the paper.

We provide **both** the data and the necessary steps to re-create it.

Below is a list of artifacts along with their dependencies, their files, and the time needed to reproduce them.


## Related Papers

* [Antoine El-Hokayem , Yliès Falcone, Monitoring decentralized specifications, Proceedings of the 26th ACM SIGSOFT International Symposium on Software Testing and Analysis, July 10-14, 2017, Santa Barbara, CA, USA](https://dl.acm.org/citation.cfm?id=3092723)

* [Antoine El-Hokayem , Yliès Falcone, THEMIS: a tool for decentralized monitoring algorithms, Proceedings of the 26th ACM SIGSOFT International Symposium on Software Testing and Analysis, July 10-14, 2017, Santa Barbara, CA, USA](https://dl.acm.org/citation.cfm?id=3098224)
